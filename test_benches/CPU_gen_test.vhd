library ieee;
use ieee.std_logic_1164.all;

entity CPU_gen_test is
end CPU_gen_test;

-- Tests the artifical CPU provided by the lab manual to see what kind of instructions it emits

architecture behavior of CPU_gen_test is

	-- Component Declaration for the Unit Under Test (UUT)

	component CPU_gen
		port (
			clk : in std_logic;
			rst : in std_logic;
			trig : in std_logic;
			Address : out std_logic_vector(15 downto 0);
			wr_rd : out std_logic;
			cs : out std_logic;
			DOut : out std_logic_vector(7 downto 0)
		);
	end component;
	--Inputs
	signal clk : std_logic := '0';
	signal rst : std_logic := '0';
	signal trig : std_logic := '0';

	--Outputs
	signal Address : std_logic_vector(15 downto 0);
	signal wr_rd : std_logic;
	signal cs : std_logic;
	signal DOut : std_logic_vector(7 downto 0);

	-- Clock period definitions
	constant clk_period : time := 10 ns;

begin

	-- Instantiate the Unit Under Test (UUT)
	uut : CPU_gen port map(
		clk => clk,
		rst => rst,
		trig => trig,
		Address => Address,
		wr_rd => wr_rd,
		cs => cs,
		DOut => DOut
	);

	-- Clock process definitions
	clk_process : process
	begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
	end process;
	-- Stimulus process
	stim_proc : process
	begin
		-- hold reset state for 100 ns.
		wait for 100 ns;

		wait for clk_period * 10;

		-- insert stimulus here 

		Trig <= '1';
		wait for clk_period * 4;
		Trig <= '0';
		wait for clk_period * 10;

		Trig <= '1';
		wait for clk_period * 4;
		Trig <= '0';
		wait for clk_period * 10;

		Trig <= '1';
		wait for clk_period * 4;
		Trig <= '0';
		wait for clk_period * 10;

		Trig <= '1';
		wait for clk_period * 4;
		Trig <= '0';
		wait for clk_period * 10;

		Trig <= '1';
		wait for clk_period * 4;
		Trig <= '0';
		wait for clk_period * 10;

		wait;
	end process;

end;