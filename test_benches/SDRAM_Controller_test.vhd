library ieee;
use ieee.std_logic_1164.all;

entity SDRAM_Controller_test is
end SDRAM_Controller_test;

-- Tests the SDRAM Controller to ensure main memory works

architecture behavior of SDRAM_Controller_test is

	-- Component Declaration for the Unit Under Test (UUT)

	component SDRAM_Controller
		port (
			clk : in std_logic;
			address : in std_logic_vector(15 downto 0);
			wr_rd : in std_logic;
			mem_strb : in std_logic;
			data_in : in std_logic_vector(7 downto 0);
			data_out : out std_logic_vector(7 downto 0)
		);
	end component;
	--Inputs
	signal clk : std_logic := '0';
	signal address : std_logic_vector(15 downto 0) := (others => '0');
	signal wr_rd : std_logic := '0';
	signal mem_strb : std_logic := '0';
	signal data_in : std_logic_vector(7 downto 0) := (others => '0');

	--Outputs
	signal data_out : std_logic_vector(7 downto 0);

	-- Clock period definitions
	constant clk_period : time := 10 ns;

begin

	-- Instantiate the Unit Under Test (UUT)
	uut : SDRAM_Controller port map(
		clk => clk,
		address => address,
		wr_rd => wr_rd,
		mem_strb => mem_strb,
		data_in => data_in,
		data_out => data_out
	);

	-- Clock process definitions
	clk_process : process
	begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
	end process;
	-- Stimulus process
	stim_proc : process
	begin
		-- hold reset state for 100 ns.
		wait for 100 ns;

		wait for clk_period * 10;

		-- insert stimulus here 

		wr_rd <= '1';

		address <= x"00A0";
		data_in <= x"D0";
		wait for clk_period * 5;
		mem_strb <= '1';
		wait for clk_period * 5;
		mem_strb <= '0';
		wait for clk_period * 5;

		address <= x"00A1";
		data_in <= x"D1";
		wait for clk_period * 5;
		mem_strb <= '1';
		wait for clk_period * 5;
		mem_strb <= '0';
		wait for clk_period * 5;

		address <= x"00A2";
		data_in <= x"D2";
		wait for clk_period * 5;
		mem_strb <= '1';
		wait for clk_period * 5;
		mem_strb <= '0';
		wait for clk_period * 5;

		wr_rd <= '0';

		address <= x"00A0";
		wait for clk_period * 5;
		mem_strb <= '1';
		wait for clk_period * 5;
		mem_strb <= '0';
		wait for clk_period * 5;

		address <= x"00A1";
		wait for clk_period * 5;
		mem_strb <= '1';
		wait for clk_period * 5;
		mem_strb <= '0';
		wait for clk_period * 5;

		address <= x"00A2";
		wait for clk_period * 5;
		mem_strb <= '1';
		wait for clk_period * 5;
		mem_strb <= '0';
		wait for clk_period * 5;

		wait;
	end process;

end;