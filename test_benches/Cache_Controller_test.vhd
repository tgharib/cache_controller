library ieee;
use ieee.std_logic_1164.all;
use work.Common.all;

entity Cache_Controller_test is
end Cache_Controller_test;

-- Tests the cache controller to ensure it has correct functionality

architecture behavior of Cache_Controller_test is

	-- Component Declaration for the Unit Under Test (UUT)

	component Cache_Controller
		port (
			clk : in std_logic;
			address : in std_logic_vector(15 downto 0);
			wr_rd : in std_logic;
			cs : in std_logic;
			data_in : in std_logic_vector(7 downto 0);
			data_out : out std_logic_vector(7 downto 0);
			ready : out std_logic;
			debugState : out stateType;
			debugTag : out std_logic_vector(7 downto 0);
			debugCacheBlockNumber : out std_logic_vector(2 downto 0);
			debugOffset : out std_logic_vector(4 downto 0)
		);
	end component;
	--Inputs
	signal clk : std_logic := '0';
	signal address : std_logic_vector(15 downto 0) := (others => '0');
	signal wr_rd : std_logic := '0';
	signal cs : std_logic := '0';
	signal data_in : std_logic_vector(7 downto 0) := (others => '0');

	--Outputs
	signal data_out : std_logic_vector(7 downto 0);
	signal ready : std_logic;
	signal debugState : stateType;
	signal debugTag : std_logic_vector(7 downto 0);
	signal debugCacheBlockNumber : std_logic_vector(2 downto 0);
	signal debugOffset : std_logic_vector(4 downto 0);

	-- Clock period definitions
	constant clk_period : time := 10 ns;

begin

	-- Instantiate the Unit Under Test (UUT)
	uut : Cache_Controller port map(
		clk => clk,
		address => address,
		wr_rd => wr_rd,
		cs => cs,
		data_in => data_in,
		data_out => data_out,
		ready => ready,
		debugState => debugState,
		debugTag => debugTag,
		debugCacheBlockNumber => debugCacheBlockNumber,
		debugOffset => debugOffset
	);

	-- Clock process definitions
	clk_process : process
	begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
	end process;
	-- Stimulus process
	stim_proc : process
	begin
		-- hold reset state for 100 ns.
		wait for 100 ns;

		wait for clk_period * 10;

		-- insert stimulus here 
		address <= x"0000"; -- store 0xA at 0x0
		wr_rd <= '1';
		data_in <= x"0A";
		cs <= '1';
		wait for clk_period * 5;
		cs <= '0';
		wait until ready = '1';
		wait for clk_period * 5;

		address <= x"0001"; -- store 0xB at 0x1
		wr_rd <= '1';
		data_in <= x"0B";
		cs <= '1';
		wait for clk_period * 5;
		cs <= '0';
		wait until ready = '1';
		wait for clk_period * 5;

		address <= x"0002"; -- store 0xC at 0x2
		wr_rd <= '1';
		data_in <= x"0C";
		cs <= '1';
		wait for clk_period * 5;
		cs <= '0';
		wait until ready = '1';
		wait for clk_period * 5;

		address <= x"0000"; -- view value at 0x0
		wr_rd <= '0';
		cs <= '1';
		wait for clk_period * 5;
		cs <= '0';
		wait until ready = '1';
		wait for clk_period * 5;

		address <= x"0001"; -- view value at 0x1
		wr_rd <= '0';
		cs <= '1';
		wait for clk_period * 5;
		cs <= '0';
		wait until ready = '1';
		wait for clk_period * 5;

		address <= x"0002"; -- view value at 0x2
		wr_rd <= '0';
		cs <= '1';
		wait for clk_period * 5;
		cs <= '0';
		wait until ready = '1';
		wait for clk_period * 5;

		address <= x"0101"; -- store 0x4 at 0x101
		wr_rd <= '1';
		data_in <= x"04";
		cs <= '1';
		wait for clk_period * 5;
		cs <= '0';
		wait until ready = '1';
		wait for clk_period * 5;

		address <= x"0001"; -- view value at 0x1
		wr_rd <= '0';
		cs <= '1';
		wait for clk_period * 5;
		cs <= '0';
		wait until ready = '1';
		wait for clk_period * 5;

		address <= x"0101"; -- view value at 0x101
		wr_rd <= '0';
		cs <= '1';
		wait for clk_period * 5;
		cs <= '0';
		wait until ready = '1';
		wait for clk_period * 5;

		wait;
	end process;

end;