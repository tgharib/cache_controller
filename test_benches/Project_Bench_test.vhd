library ieee;
use ieee.std_logic_1164.all;

entity Project_Bench_test is
end Project_Bench_test;

-- Tests the entire project (CPU + cache controller) to ensure it has correct functionality

architecture behavior of Project_Bench_test is

	-- Component Declaration for the Unit Under Test (UUT)

	component Project_Bench
		port (
			clk : in std_logic;
			projectReset : in std_logic;
			projectTrigger : in std_logic;
			projectCPUAddress : out std_logic_vector(15 downto 0);
			projectCPUWr_rd : out std_logic;
			projectCPUCs : out std_logic;
			projectCacheControllerDataOut : out std_logic_vector(7 downto 0);
			projectCacheControllerReady : out std_logic
		);
	end component;
	--Inputs
	signal clk : std_logic := '0';
	signal projectReset : std_logic := '0';
	signal projectTrigger : std_logic := '0';

	--Outputs
	signal projectCPUAddress : std_logic_vector(15 downto 0);
	signal projectCPUWr_rd : std_logic;
	signal projectCPUCs : std_logic;
	signal projectCacheControllerDataOut : std_logic_vector(7 downto 0);
	signal projectCacheControllerReady : std_logic;

	-- Clock period definitions
	constant clk_period : time := 10 ns;

begin

	-- Instantiate the Unit Under Test (UUT)
	uut : Project_Bench port map(
		clk => clk,
		projectReset => projectReset,
		projectTrigger => projectTrigger,
		projectCPUAddress => projectCPUAddress,
		projectCPUWr_rd => projectCPUWr_rd,
		projectCPUCs => projectCPUCs,
		projectCacheControllerDataOut => projectCacheControllerDataOut,
		projectCacheControllerReady => projectCacheControllerReady
	);

	-- Clock process definitions
	clk_process : process
	begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
	end process;
	-- Stimulus process
	stim_proc : process
	begin
		-- hold reset state for 100 ns.
		wait for 100 ns;

		wait for clk_period * 10;

		-- insert stimulus here 
		projectTrigger <= '1';
		wait for clk_period * 4;
		projectTrigger <= '0';
		wait until projectCacheControllerReady = '1';
		wait for clk_period * 4;

		projectTrigger <= '1';
		wait for clk_period * 4;
		projectTrigger <= '0';
		wait until projectCacheControllerReady = '1';
		wait for clk_period * 4;

		projectTrigger <= '1';
		wait for clk_period * 4;
		projectTrigger <= '0';
		wait until projectCacheControllerReady = '1';
		wait for clk_period * 4;

		projectTrigger <= '1';
		wait for clk_period * 4;
		projectTrigger <= '0';
		wait until projectCacheControllerReady = '1';
		wait for clk_period * 4;

		projectTrigger <= '1';
		wait for clk_period * 4;
		projectTrigger <= '0';
		wait until projectCacheControllerReady = '1';
		wait for clk_period * 4;

		projectTrigger <= '1';
		wait for clk_period * 4;
		projectTrigger <= '0';
		wait until projectCacheControllerReady = '1';
		wait for clk_period * 4;

		projectTrigger <= '1';
		wait for clk_period * 4;
		projectTrigger <= '0';
		wait until projectCacheControllerReady = '1';
		wait for clk_period * 4;

		projectTrigger <= '1';
		wait for clk_period * 4;
		projectTrigger <= '0';
		wait until projectCacheControllerReady = '1';
		wait for clk_period * 4;

		wait;
	end process;

end;