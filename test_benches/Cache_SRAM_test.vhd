library ieee;
use ieee.std_logic_1164.all;

entity Cache_SRAM_test is
end Cache_SRAM_test;

-- Tests the Cache SRAM Block RAM (IP Core) to ensure Cache memory works

architecture behavior of Cache_SRAM_test is

	-- Component Declaration for the Unit Under Test (UUT)

	component Cache_SRAM
		port (
			clka : in std_logic;
			wea : in std_logic_vector(0 downto 0);
			addra : in std_logic_vector(7 downto 0);
			dina : in std_logic_vector(7 downto 0);
			douta : out std_logic_vector(7 downto 0)
		);
	end component;
	--Inputs
	signal clka : std_logic := '0';
	signal wea : std_logic_vector(0 downto 0) := (others => '0');
	signal addra : std_logic_vector(7 downto 0) := (others => '0');
	signal dina : std_logic_vector(7 downto 0) := (others => '0');

	--Outputs
	signal douta : std_logic_vector(7 downto 0);

	-- Clock period definitions
	constant clka_period : time := 10 ns;

begin

	-- Instantiate the Unit Under Test (UUT)
	uut : Cache_SRAM port map(
		clka => clka,
		wea => wea,
		addra => addra,
		dina => dina,
		douta => douta
	);

	-- Clock process definitions
	clka_process : process
	begin
		clka <= '0';
		wait for clka_period/2;
		clka <= '1';
		wait for clka_period/2;
	end process;
	-- Stimulus process
	stim_proc : process
	begin
		-- hold reset state for 100 ns.
		wait for 100 ns;

		wait for clka_period * 10;

		-- insert stimulus here
		wea <= "1";

		addra <= x"A0";
		dina <= x"D0";
		wait for clka_period * 2;

		addra <= x"A1";
		dina <= x"D1";
		wait for clka_period * 2;

		addra <= x"A2";
		dina <= x"D2";
		wait for clka_period * 2;

		wea <= "0";

		addra <= x"A0";
		wait for clka_period * 2;

		addra <= x"A1";
		wait for clka_period * 2;

		addra <= x"A2";
		wait for clka_period * 2;

		wait;
	end process;

end;