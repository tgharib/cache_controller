library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity SDRAM_Controller is
	port (
		clk : in std_logic;
		address : in std_logic_vector(15 downto 0);
		wr_rd : in std_logic;
		mem_strb : in std_logic;
		data_in : in std_logic_vector(7 downto 0);
		data_out : out std_logic_vector(7 downto 0));
end SDRAM_Controller;

architecture Arch of SDRAM_Controller is
	-- sdram
	component SDRAM_Block
		port (
			clka : in std_logic;
			wea : in std_logic_vector(0 downto 0);
			addra : in std_logic_vector(14 downto 0);
			dina : in std_logic_vector(7 downto 0);
			douta : out std_logic_vector(7 downto 0));
	end component;

	-- signals for sdram interface
	signal sdram_wen : std_logic_vector(0 downto 0);
	signal sdram_addr : std_logic_vector(14 downto 0);
	signal sdram_data_in : std_logic_vector(7 downto 0);
	signal sdram_data_out : std_logic_vector(7 downto 0);
begin
	sdram : SDRAM_Block port map(clk, sdram_wen, sdram_addr, sdram_data_in, sdram_data_out);

	process (clk)begin
		if mem_strb = '1' then
			if rising_edge(clk) then
				if wr_rd = '0' then --if reading from memory
					sdram_wen <= "0";
					sdram_addr <= address(14 downto 0);
					data_out <= sdram_data_out;
				elsif wr_rd = '1' then --if writing to memory
					sdram_wen <= "1";
					sdram_addr <= address(14 downto 0);
					sdram_data_in <= data_in;
					data_out <= x"00";
				end if;
			end if;
		end if;
	end process;
end Arch;