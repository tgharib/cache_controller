library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.Common.all;

entity Cache_Controller is
  port (
    clk : in std_logic;
    address : in std_logic_vector(15 downto 0);
    wr_rd : in std_logic;
    cs : in std_logic;
    data_in : in std_logic_vector(7 downto 0);
    data_out : out std_logic_vector(7 downto 0);
    ready : out std_logic;
    debugState : out stateType;
    debugTag : out std_logic_vector(7 downto 0);
    debugCacheBlockNumber : out std_logic_vector(2 downto 0);
    debugOffset : out std_logic_vector(4 downto 0);
    debugSdram_data_in : out std_logic_vector(7 downto 0);
    debugSdram_data_out : out std_logic_vector(7 downto 0);
    debugCache_data_in : out std_logic_vector(7 downto 0);
    debugMem_strb : out std_logic);
end Cache_Controller;

architecture arch of Cache_Controller is
  -- cache
  component Cache_SRAM
    port (
      clka : in std_logic;
      wea : in std_logic_vector(0 downto 0);
      addra : in std_logic_vector(7 downto 0);
      dina : in std_logic_vector(7 downto 0);
      douta : out std_logic_vector(7 downto 0));
  end component;

  -- signals for cache interface
  signal cache_wen : std_logic_vector(0 downto 0);
  signal cache_address : std_logic_vector(7 downto 0);
  signal cache_data_in : std_logic_vector(7 downto 0);
  signal cache_data_out : std_logic_vector(7 downto 0);

  -- memory
  component SDRAM_Controller is
    port (
      clk : in std_logic;
      address : in std_logic_vector(15 downto 0);
      wr_rd : in std_logic;
      mem_strb : in std_logic;
      data_in : in std_logic_vector(7 downto 0);
      data_out : out std_logic_vector(7 downto 0));
  end component;

  -- signals for memory interface
  signal sdram_address : std_logic_vector(15 downto 0);
  signal sdram_wr_rd : std_logic;
  signal sdram_mem_strb : std_logic;
  signal sdram_data_in : std_logic_vector(7 downto 0);
  signal sdram_data_out : std_logic_vector(7 downto 0);

  -- address fields
  alias offset : std_logic_vector(4 downto 0) is address(4 downto 0);
  alias cacheBlockNumber : std_logic_vector(2 downto 0) is address(7 downto 5);
  alias tag : std_logic_vector(7 downto 0) is address(15 downto 8);
begin
  cache : Cache_SRAM port map(clk, cache_wen, cache_address, cache_data_in, cache_data_out);
  sdram : SDRAM_Controller port map(clk, sdram_address, sdram_wr_rd, sdram_mem_strb, sdram_data_in, sdram_data_out);

  process (clk)
    -- cache block identifiers
    type blockStruct is record
      tag : std_logic_vector(7 downto 0);
      dirtyBit : std_logic;
      validBit : std_logic;
    end record blockStruct;

    constant blockStructInit : blockStruct := (tag => (others => '0'), dirtyBit => '0', validBit => '0');
    type blockArrayType is array (0 to 7) of blockStruct;
    variable blockArray : blockArrayType := (others => blockStructInit);

    -- state variables for main FSM
    variable state : stateType := UNKNOWN;
    variable cyclesToWait : integer := 0;
    variable prevStateBeforeWait : stateType := UNKNOWN;
    variable nextStateAfterWait : stateType := UNKNOWN;
    variable cacheHit : std_logic := '0';
    variable nextByteMemToCache : integer := 0;
    variable nextByteCacheToMem : integer := 0;

    -- state variables for secondary FSM (for an operation over multiple cycles)
    variable state1 : stateType := UNKNOWN; -- state after CACHE_TO_MEM
    variable state2 : stateType := UNKNOWN; -- state after MEM_TO_CACHE
  begin
    debugState <= state;
    debugTag <= tag;
    debugCacheBlockNumber <= cacheBlockNumber;
    debugOffset <= offset;

    if rising_edge(clk) then
      sdram_mem_strb <= '0'; --unsure if this is the right place to put this
      case state is
        when UNKNOWN =>
          state := READYST;

        when WAIT_CYCLES =>
          -- While waiting, update values as per the previous state
          if prevStateBeforeWait = CACHE_TO_MEM then
            sdram_data_in <= cache_data_out;
          elsif prevStateBeforeWait = MEM_TO_CACHE then
            cache_data_in <= sdram_data_out;
          elsif prevStateBeforeWait = READ_BYTE then
            data_out <= cache_data_out;
          elsif prevStateBeforeWait = WRITE_BYTE then
            cache_data_in <= data_in;
          end if;

          if cyclesToWait > 0 then
            cyclesToWait := cyclesToWait - 1;
          else
            state := nextStateAfterWait;
          end if;

        when READYST =>
          ready <= '1';

          if cs = '1' then -- cpu is ready to send next operation
            ready <= '0';
            cacheHit := '0';

            for i in blockArray'range loop
              if (i = to_integer(unsigned(cacheBlockNumber))) and (blockArray(i).tag = tag) and (blockArray(i).validBit = '1') then -- cache hit
                cacheHit := '1';

                if (wr_rd = '0') then -- read
                  state := READ_BYTE;
                else -- write
                  state := WRITE_BYTE;
                end if;
                exit; -- no need to loop through the remaining cache blocks
              end if;
            end loop;

            if cacheHit = '0' then -- cache miss
              for i in blockArray'range loop
                if i = to_integer(unsigned(cacheBlockNumber)) then -- find block
                  if ((blockArray(i).validBit = '1') and (blockArray(i).dirtyBit = '0')) or (blockArray(i).validBit = '0') then -- dirty bit = 0
                    if (wr_rd = '0') then -- read
                      -- Step 1: MEM_TO_CACHE: block in memory --> block in cache
                      nextByteMemToCache := 0;
                      state := MEM_TO_CACHE;
                      -- Step 2: READ_BYTE: data output = cache(offset)
                      state2 := READ_BYTE;
                    else -- write
                      -- Step 1: MEM_TO_CACHE: block in memory --> block in cache
                      nextByteMemToCache := 0;
                      state := MEM_TO_CACHE;
                      -- Step 2: WRITE_BYTE: cache(offset) = data_input
                      state2 := WRITE_BYTE;
                    end if;
                  else -- dirty bit = 1
                    if (wr_rd = '0') then -- read
                      -- Step 1: CACHE_TO_MEM: block in cache --> block in memory
                      nextByteCacheToMem := 0;
                      state := CACHE_TO_MEM;
                      -- Step 2: MEM_TO_CACHE: block in memory --> block in cache
                      nextByteMemToCache := 0;
                      state1 := MEM_TO_CACHE;
                      -- Step 3: READ_BYTE: data output = cache(offset)
                      state2 := READ_BYTE;
                    else -- write
                      -- Step 1: CACHE_TO_MEM: block in cache --> block in memory
                      nextByteCacheToMem := 0;
                      state := CACHE_TO_MEM;
                      -- Step 2: MEM_TO_CACHE: block in memory --> block in cache
                      nextByteMemToCache := 0;
                      state1 := MEM_TO_CACHE;
                      -- Step 3: WRITE_BYTE: cache(offset) = data_input
                      state2 := WRITE_BYTE;
                    end if;
                  end if;
                  exit; -- no need to loop through the remaining cache blocks
                end if;
              end loop;
            end if;
          end if;

        when CACHE_TO_MEM =>
          blockArray(to_integer(unsigned(cacheBlockNumber))).dirtyBit := '0';

          if nextByteCacheToMem < 32 then
            sdram_address <= blockArray(to_integer(unsigned(cacheBlockNumber))).tag & cacheBlockNumber & std_logic_vector(to_unsigned(nextByteCacheToMem, 5));
            sdram_wr_rd <= '1';
            sdram_mem_strb <= '1';
            sdram_data_in <= cache_data_out;

            cache_address <= cacheBlockNumber & std_logic_vector(to_unsigned(nextByteCacheToMem, 5));
            cache_wen <= "0";

            nextByteCacheToMem := nextByteCacheToMem + 1;
            cyclesToWait := 3;
            prevStateBeforeWait := CACHE_TO_MEM;
            nextStateAfterWait := CACHE_TO_MEM;
            state := WAIT_CYCLES;
          else
            state := state1;
          end if;

        when MEM_TO_CACHE =>
          blockArray(to_integer(unsigned(cacheBlockNumber))).tag := tag;
          blockArray(to_integer(unsigned(cacheBlockNumber))).dirtyBit := '0';
          blockArray(to_integer(unsigned(cacheBlockNumber))).validBit := '1';

          if nextByteMemToCache < 32 then
            sdram_address <= tag & cacheBlockNumber & std_logic_vector(to_unsigned(nextByteMemToCache, 5));
            sdram_wr_rd <= '0';
            sdram_mem_strb <= '1';

            cache_address <= cacheBlockNumber & std_logic_vector(to_unsigned(nextByteMemToCache, 5));
            cache_wen <= "1";
            cache_data_in <= sdram_data_out;

            nextByteMemToCache := nextByteMemToCache + 1;
            cyclesToWait := 3;
            prevStateBeforeWait := MEM_TO_CACHE;
            nextStateAfterWait := MEM_TO_CACHE;
            state := WAIT_CYCLES;
          else
            state := state2;
          end if;

        when READ_BYTE =>
          cache_address <= cacheBlockNumber & offset;
          cache_wen <= "0";
          data_out <= cache_data_out;

          cyclesToWait := 1;
          prevStateBeforeWait := READ_BYTE;
          nextStateAfterWait := READYST;
          state := WAIT_CYCLES;

        when WRITE_BYTE =>
          blockArray(to_integer(unsigned(cacheBlockNumber))).dirtyBit := '1';

          cache_address <= cacheBlockNumber & offset;
          cache_wen <= "1";
          cache_data_in <= data_in;

          cyclesToWait := 1;
          prevStateBeforeWait := WRITE_BYTE;
          nextStateAfterWait := READYST;
          state := WAIT_CYCLES;

        when others =>
          state := UNKNOWN;
      end case;
    end if;

  debugSdram_data_in <= sdram_data_in;
  debugSdram_data_out <= sdram_data_out;
  debugCache_data_in <= cache_data_in;
  debugMem_strb <= sdram_mem_strb;

  end process;
end arch;
