library ieee;
use ieee.std_logic_1164.all;
use work.Common.all;

entity Project_Bench is
	port (
		clk : in std_logic;
		projectReset : in std_logic;
		projectTrigger : in std_logic;
		projectCPUAddress : out std_logic_vector(15 downto 0);
		projectCPUWr_rd : out std_logic;
		projectCPUCs : out std_logic;
		projectCacheControllerDataOut : out std_logic_vector(7 downto 0);
		projectCacheControllerReady : out std_logic);
end Project_Bench;

architecture arch of Project_Bench is
	-- cpu
	component CPU_gen
		port (
			clk : in std_logic;
			rst : in std_logic;
			trig : in std_logic;
			Address : out std_logic_vector(15 downto 0);
			wr_rd : out std_logic;
			cs : out std_logic;
			DOut : out std_logic_vector(7 downto 0));
	end component;

	-- signals for cpu interface
	signal cpu_reset : std_logic;
	signal cpu_trigger : std_logic;
	signal cpu_Address : std_logic_vector(15 downto 0);
	signal cpu_wr_rd : std_logic;
	signal cpu_cs : std_logic;
	signal cpu_Dout : std_logic_vector(7 downto 0);

	-- cache controller
	component Cache_Controller
		port (
			clk : in std_logic;
			address : in std_logic_vector(15 downto 0);
			wr_rd : in std_logic;
			cs : in std_logic;
			data_in : in std_logic_vector(7 downto 0);
			data_out : out std_logic_vector(7 downto 0);
			ready : out std_logic;
			debugState : out stateType;
			debugTag : out std_logic_vector(7 downto 0);
			debugCacheBlockNumber : out std_logic_vector(2 downto 0);
			debugOffset : out std_logic_vector(4 downto 0);
			debugSdram_data_in : out std_logic_vector(7 downto 0);
			debugSdram_data_out : out std_logic_vector(7 downto 0);
			debugCache_data_in : out std_logic_vector(7 downto 0);
			debugMem_strb : out std_logic);
	end component;

	-- signals for cache controller interface
	signal cache_controller_ready : std_logic;
	signal cache_controller_data_out : std_logic_vector(7 downto 0);
	signal cache_controller_debugState : stateType; -- unused
	signal cache_controller_debugTag : std_logic_vector(7 downto 0); -- unused
	signal cache_controller_debugCacheBlockNumber : std_logic_vector(2 downto 0); -- unused
	signal cache_controller_debugOffset : std_logic_vector(4 downto 0); -- unused
	signal cache_controller_debugSdram_data_in : std_logic_vector(7 downto 0);
	signal cache_controller_debugSdram_data_out : std_logic_vector(7 downto 0);
	signal cache_controller_debugCache_data_in : std_logic_vector(7 downto 0);
	signal cache_controller_debugMem_strb : std_logic;

	-- Chipscope
	component icon
		port (CONTROL0 : inout std_logic_vector(35 downto 0));
	end component;

	component ila
		port (
			CONTROL : inout std_logic_vector(35 downto 0);
			CLK : in std_logic;
			DATA : in std_logic_vector(63 downto 0);
			TRIG0 : in std_logic_vector(7 downto 0));
	end component;

	signal control0 : std_logic_vector(35 downto 0);
	signal ila_data : std_logic_vector(63 downto 0);
	signal trig0 : std_logic_vector(7 downto 0);
begin
	cpu_reset <= projectReset;
	cpu_trigger <= projectTrigger;
	projectCPUAddress <= cpu_Address;
	projectCPUWr_rd <= cpu_wr_rd;
	projectCPUCs <= cpu_cs;
	projectCacheControllerDataOut <= cache_controller_data_out;
	projectCacheControllerReady <= cache_controller_ready;

	cpu : CPU_gen port map(clk, cpu_reset, cpu_trigger, cpu_Address, cpu_wr_rd, cpu_cs, cpu_Dout);

	cacheController : Cache_Controller port map(
		clk, cpu_Address, cpu_wr_rd, cpu_cs, cpu_Dout, cache_controller_data_out, cache_controller_ready,
		cache_controller_debugState, cache_controller_debugTag, cache_controller_debugCacheBlockNumber,
		cache_controller_debugOffset, cache_controller_debugSdram_data_in, cache_controller_debugSdram_data_out,
		cache_controller_debugCache_data_in, cache_controller_debugMem_strb);

	-- Chipscope
	projectIcon : icon port map(control0);
	projectIla : ila port map(control0, clk, ila_data, trig0);

	ila_data(0) <= projectReset;
	ila_data(1) <= projectTrigger;
	ila_data(17 downto 2) <= cpu_Address;
	ila_data(18) <= cpu_wr_rd;
	ila_data(19) <= cpu_cs;
	ila_data(27 downto 20) <= cache_controller_data_out;
	ila_data(28) <= cache_controller_ready;
	ila_data(36 downto 29) <= cache_controller_debugSdram_data_in;
	ila_data(44 downto 37) <= cache_controller_debugSdram_data_out;
	ila_data(52 downto 45) <= cache_controller_debugCache_data_in;
	ila_data(53) <= cache_controller_debugMem_strb;
	ila_data(63 downto 54) <= (others => '0'); -- unused

	trig0(0) <= projectTrigger;
	trig0(7 downto 1) <= (others => '0'); -- unused
end arch;
