package Common is
	type stateType is (UNKNOWN, WAIT_CYCLES, READYST, CACHE_TO_MEM, MEM_TO_CACHE, READ_BYTE, WRITE_BYTE);
end Common;

package body Common is
end Common;