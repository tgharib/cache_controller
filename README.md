This cache controller was written in original VHDL for the completion of the lab assignments in this course: https://www.ecb.torontomu.ca/~lkirisch/coe758/labs.htm

## Problems with the design
* Since the process statement inside the Cache controller is only activated on the rising edge of the clock, there is always a wasted clock cycle when transitioning between states in the FSM. This can be solved by adding state to the process sensitivity list to immediately switch states (and then WAIT_CYCLES can be used if there needs to be a delay).
* Inside the Cache controller, instead of waiting a certain amount of cycles in WAIT\_CYCLES to switch between states, WAIT\_CYCLES should wait on probe signals from the SRAM or SDRAM controller or CPU.
* There are unnecessary for loops inside the cache controller. Once the cache block number is known from the memory address, it is only required to compare the tags instead of looping through all the cache blocks.

## Specifications

Cache size = 256 bytes

Block size = 32 words/bytes (since we define 1 word = 1 byte). Therefore, b=5.

Therefore, 8 blocks are needed for 1 cache. Therefore, c=3.

In CPU, a memory address (called an address word) is 16 bits. Therefore, d=16. The SDRAM only implements 15 bits of memory and discards the most significant bit.

## Cache Controller Cases

1. Write a word on cache [hit]. Set the dirty bit to 1.

2. Read a word on cache [hit].

3. Read/write a word on cache [miss] and dirty bit is 0. Move a block from memory to cache and set the valid bit to 1. Read/write a word on cache.

4. Read/write a word cache [miss] and dirty bit is 1. Move a block from cache to memory, set the dirty bit to 0, move a block from memory to cache and set the valid bit to 1. Read/write a word on cache.
